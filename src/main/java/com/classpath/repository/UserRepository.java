package com.classpath.repository;

import com.classpath.model.User;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

public class UserRepository {

    private final Set<User> users = new HashSet<>();

    public User saveUser(User user){
        user.setUserId((int)(Math.ceil(Math.random()* 234234234)));
        this.users.add(user);
        return user;
    }

    public Set<User> fetchAllUsers(){
        return unmodifiableSet(this.users);
    }

    public Optional<User> fetchUserByUserId(int userId){
        return this.users.stream().filter(user -> user.getUserId() == userId).findFirst();
    }

    public void deleteUserById(int userId){
        this.users.removeIf(user -> user.getUserId() == userId);
    }
}