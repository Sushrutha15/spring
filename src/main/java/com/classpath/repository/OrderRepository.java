package com.classpath.repository;

import com.classpath.model.Order;
import com.classpath.model.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

public class OrderRepository {
    private final Set<Order> orders= new HashSet<>();

    public Order saveOrder(Order order) {
        order.setOrderId((int)(Math.ceil(Math.random()* 234234234)));
        this.orders.add(order);
        return order;
    }

    public Set<Order> fetchAllOrders(){
        return unmodifiableSet(this.orders);
    }

    public Optional<Order> fetchOrderByOrderId(int Id)
    {
        return this.orders.stream().filter(order -> order.getOrderId() == Id).findFirst();
    }
    public void deleteOrderById(int orderId)
    {
        this.orders.removeIf(order -> order.getOrderId()==orderId);
    }
}
