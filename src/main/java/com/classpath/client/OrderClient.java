package com.classpath.client;

import com.classpath.model.Order;
import com.classpath.service.OrderService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Optional;

public class OrderClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        final OrderService orderService = applicationContext.getBean("orderService", OrderService.class);

        orderService.saveOrder(Order.builder().orderName("Ear Pods").build());
        orderService.saveOrder(Order.builder().orderName("Mobiles").build());
        orderService.saveOrder(Order.builder().orderName("Laptops").build());
        orderService.saveOrder(Order.builder().orderName("Watches").build());
        orderService.saveOrder(Order.builder().orderName("Desktops").build());

        //fetch all users
        orderService.fetchAllOrders().forEach(System.out::println);
        //fetch user by id
        Order order = orderService.fetchAllOrders().stream().findFirst().get();
        Optional<Order> fetchedOrder = orderService.fetchOrderByOrderId(order.getOrderId());
        System.out.println("Fetched Order :: "+ fetchedOrder);

        System.out.println("Number of Orders before deleting :: " + orderService.fetchAllOrders().size());
        orderService.deleteOrderById(order.getOrderId());
        System.out.println("Number of Orders after deleting :: " + orderService.fetchAllOrders().size());
    }
}
