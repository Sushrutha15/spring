package com.classpath.model;

public interface UberGo {

    public double commute(String from, String destination);
}