package com.classpath.service;

import com.classpath.model.User;
import com.classpath.repository.UserRepository;

import java.util.Set;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User saveUser(User user){
        return this.userRepository.saveUser(user);
    }

    public Set<User> fetchAllUsers(){
        return this.userRepository.fetchAllUsers();
    }

    public User fetchUserByUserId(int userId){
        return this.userRepository
                        .fetchUserByUserId(userId)
                        .orElseThrow(UserService::invalidUser);
    }

    public void deleteUserById(int userId){
        this.userRepository.deleteUserById(userId);
    }

    private static IllegalArgumentException invalidUser() {
        return new IllegalArgumentException("Invalid user Id");
    }

}