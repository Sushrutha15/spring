package com.classpath.service;

import com.classpath.model.Order;
import com.classpath.repository.OrderRepository;

import java.util.Optional;
import java.util.Set;

public class OrderService {
    private final OrderRepository orderRepository;
    private Order order;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order saveOrder(Order order)
    {
        return this.orderRepository.saveOrder(order);
    }

    public Set<Order> fetchAllOrders()
    {
        return this.orderRepository.fetchAllOrders();
    }

    public Optional<Order> fetchOrderByOrderId(int orderId)
    {
        return this.orderRepository.fetchOrderByOrderId(orderId);
    }

    public void deleteOrderById(int orderId)
    {
        this.orderRepository.deleteOrderById(orderId);
    }
}
